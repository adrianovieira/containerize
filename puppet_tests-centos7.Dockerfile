FROM centos:7

LABEL maintainer 'Adriano Vieira <adriano.svieira at gmail.com>'

ADD setup/puppet_tests/Gemfile /usr/src/puppet_tests/Gemfile

ENV LANG="en_US.UTF-8"

# check pacake names on:
#  https://pkgs.alpinelinux.org/packages
#  http://dl-3.alpinelinux.org/alpine/
# install base packs for puppet tests
RUN yum install -y epel-release; yum install -y epel-release; \
    yum install -y git gcc make ruby-devel rubygem-bundler; \
    bundle install --gemfile=/usr/src/puppet_tests/Gemfile --clean --no-cache --no-prune
