FROM debian:sid

LABEL maintainer 'Adriano Vieira <adriano.svieira at gmail.com>'

ENV LANG="en_US.UTF-8"
ENV PUPPET_VERSION='~> 3.8.0'

RUN apt-get update; \
    apt-get install -y pandoc texlive; \
    apt-get clean
