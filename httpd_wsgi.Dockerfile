FROM centos:7

LABEL maintainer 'Adriano Vieira <adriano.svieira at gmail.com>'

# install base packs
# apache.wsgi python pip
RUN yum -y install epel-release; yum -y install epel-release; \
    yum -y install python-pip httpd mod_wsgi mysql-connector-python; \
    yum clean all; \
    pip install --upgrade --no-cache-dir pip

# Simple startup script to avoid some issues observed with container restart (CentOS tip)
ADD setup/run-apache-httpd.sh /run-apache-httpd.sh
RUN chmod -v +x /run-apache-httpd.sh

# setup apache default wsgi vhost
ONBUILD COPY setup/httpd-vhost-wsgi.conf /etc/httpd/conf.d/welcome.conf

# expose apache port
ONBUILD EXPOSE 80

# pip install app requirements
ONBUILD COPY code/requirements.txt .
ONBUILD RUN yum -y install python-devel mysql-devel gcc; \
            pip install --no-cache-dir -r requirements.txt; \
            yum autoremove -y python-devel mysql-devel gcc; yum clean all;

# upload app to image
ONBUILD ADD code/contacts /var/www/html
ONBUILD WORKDIR /var/www/html

# run flask app on apache
ONBUILD CMD ["/run-apache-httpd.sh"]
