FROM debian:8

LABEL maintainer 'Adriano Vieira <adriano.svieira at gmail.com>'

ADD setup/puppet_tests/Gemfile /usr/src/puppet_tests/Gemfile

ENV LANG="en_US.UTF-8"
ENV PUPPET_VERSION='~> 3.8.0'

# check pacake names on:
#  https://pkgs.alpinelinux.org/packages
#  http://dl-3.alpinelinux.org/alpine/
# install base packs for puppet tests
RUN apt-get update; \
    apt-get install -y bundler git curl; \
    bundle install --gemfile=/usr/src/puppet_tests/Gemfile --clean --no-cache --no-prune; \
    apt-get clean
